﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using DokanNet;
using DokanNet.Logging;
using static DokanNet.FormatProviders;
using FileAccess = DokanNet.FileAccess;
using System.Timers;

namespace Dokan_Mirror_Service
{
    public partial class Mirror_Service : ServiceBase
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private char curDriverLetter = Properties.Settings.Default.StartDrive;
        private Timer timer = new Timer();
        public Mirror_Service()
        {            
             InitializeComponent();                
        }

        public void StartMirror()
        {
            log.InfoFormat("Starting application");
            var mount_paths = Properties.Settings.Default.MirrorPaths.Split(';');
            foreach (var curMount in mount_paths)
            {
                char driveLetter = getDriveLetter();
                try
                {
                    log.InfoFormat("Mapping drive {0} to {1}", driveLetter, curMount);
                    string fullmountpath = string.Format("{0}:\\", driveLetter);
                    Notify.Start(curMount, fullmountpath);
                    var curMirror = new Mirror(curMount);
                    curMirror.Mount(fullmountpath, DokanOptions.DebugMode | DokanOptions.EnableNotificationAPI, 5);
                    log.InfoFormat("Mapped drive {0} to {1}", driveLetter, curMount);
                }
                catch (Exception e)
                {
                   
                    log.ErrorFormat("Failed to map drive {0} to {1}", driveLetter, curMount);
                    log.ErrorFormat("An error {2} stack trace [{3}] occured mapping drive {0} to {1}", driveLetter, curMount,e.Message,e.StackTrace);
                }
            }
        }
        protected override void OnStart(string[] args)
        {
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 5000;
            timer.Enabled = true;
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            timer.Enabled = false;
            StartMirror();
        }

        protected override void OnStop()
        {
        }

        private char getDriveLetter()
        {
            char retval = curDriverLetter;
            if ( retval == 'Z' || retval == 'z')
            {
                curDriverLetter = 'A';
            } else
            {
                int AsciCode = (int)curDriverLetter;
                AsciCode++;
                curDriverLetter = (char)AsciCode;
            }
            return retval;
        }
    }
}
