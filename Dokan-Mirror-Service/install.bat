set APPPATH=%CD%
set MYSERVICE=mapdrives
set MYSERVICE_DISPLAY_NAME=Dokan Drive Mapper

echo "cd %APPPATH%"
cd "%APPPATH%"
net start | findstr "%MYSERVICE_DISPLAY_NAME%"
if ERRORLEVEL 0 net stop "%MYSERVICE%"

sc query state= all | findstr /C:"SERVICE_NAME: %MYSERVICE%"
IF ERRORLEVEL 0 goto createservice ELSE goto deleteservice
:createservice
	Echo "Installing Service %MYSERVICE%"
	echo sc create %MYSERVICE% start= auto binpath= "%APPPATH%\Dokan-Mirror-Service.exe" displayname= "%MYSERVICE_DISPLAY_NAME%"
	sc create %MYSERVICE% start= auto binpath= "%APPPATH%\Dokan-Mirror-Service.exe" displayname= "%MYSERVICE_DISPLAY_NAME%"
goto :end
:deleteservice
	Echo "Deleting Service %MYSERVICE% (You will need to rerun to install)"
	sc delete %MYSERVICE% 
:end
rem pause