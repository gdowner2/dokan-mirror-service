﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Dokan_Mirror_Service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            bool consoleApp = Properties.Settings.Default.ConsoleApp;
            if (!consoleApp)
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
            {
                new Mirror_Service()
            };

                ServiceBase.Run(ServicesToRun);
            } else
            {
                var mirror = new Mirror_Service();
                mirror.StartMirror();
                var instr = System.Console.ReadLine();
            }
        }
    }
}
